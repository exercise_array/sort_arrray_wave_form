import java.util.Arrays;

public class Main {


    // A utility method to swap two numbers.
    public static void swap(int arr[], int a, int b) {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

    public static void sortInWave(int arr[], int n) {
        for (int i = 0; i < n; i += 2) {

            // Sure arr[i] <= arr[i - 1
            if (i > 0 && arr[i - 1] > arr[i])
                swap(arr, i - 1, i);

            // Make sure arr[i] >= arr[i + 1]
            if (i < n - 1 && arr[i] < arr[i + 1])
                swap(arr, i, i + 1);
        }
    }

    public static void sortInWaveSorted(int arr[], int n) {
        Arrays.sort(arr);
        int i = 0;
        while (i < n - 1) {
            swap(arr, i, i + 1);
            i += 2;
        }
    }

    public static void main(String[] args) {
        int arr[] = {4, 1, 8, 5, 9};
        int arr1[] = {6, 2, 4, 9, 10, 100, 55, 2, 77};
        int n = arr.length;
        sortInWave(arr1, arr1.length);
        sortInWaveSorted(arr, n);
        for (int i : arr1)
            System.out.print(i + " ");

    }
}
